﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SMTPtoSMSgw.SMS;
using SMTPtoSMSgw.SMTP;

namespace SMTPtoSMSgw
{
	class Program
	{
		private static List<Task> _tasks = new List<Task>();

		static void Main(string[] args)
		{
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 25);
			IServer smsSender = new SmsSender();
			IServer handler = new SmtpServer(new TcpListener(endPoint), (SmsSender)smsSender);
			_tasks.Add(Task.Run(() => smsSender.Run()));
			_tasks.Add(Task.Run(() => handler.Run()));

			while (true)
			{
				var data = Console.ReadLine();

				if (data.ToUpper() == "QUIT")
				{
					handler.Stop();
					smsSender.Stop();
					break;
				}
			}

			Task.WaitAll(_tasks.ToArray());
		}
	}
}
