﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMTPtoSMSgw.SMS
{
	public struct Message
	{
		public string Number { get; set; }
		public string Text { get; set; }
	}
}
