﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace SMTPtoSMSgw.SMS
{
	public class SmsSender : IServer
	{
		private const int NoMessageWaitTime = 1000;
		private const int WaitTimeInBetweenMessages = 5000;

		private Queue<Message> _queue = new Queue<Message>();
		private bool _continue = true;
		private SerialPort _serialPort;

		public SmsSender()
		{
			_serialPort = new SerialPort("COM1", 115200);
			_serialPort.Open();
			Send("AT+CMGF=1");
		}

		public void Send(Message message)
		{
			_queue.Enqueue(message);
		}

		public void Run()
		{
			while (_continue)
			{

				if (_queue.Count == 0)
				{
					//Console.WriteLine($"SMS:No SMS found. Waiting for {NoMessageWaitTime} ms...");
					Thread.Sleep(NoMessageWaitTime);
					continue;
				}

				var message = _queue.Dequeue();
				Send("AT+CMGS=\"" + message.Number + "\"");
				Send(message.Text + "\x1A");
				Console.WriteLine("SMS: Message Sent");
				Thread.Sleep(WaitTimeInBetweenMessages);
			}
			_serialPort.Close();
		}

		public void Stop() => _continue = false;

		private bool Send(string atCommand)
		{
			_serialPort.Write(atCommand + "\r");
			Thread.Sleep(1000);
			var result = _serialPort.ReadExisting();
			Console.WriteLine(result);
			return true;
		}
	}
}
