﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SMTPtoSMSgw
{
	interface IServer
	{
		void Run();
		void Stop();
	}
}
