﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SMTPtoSMSgw.Extensions;
using SMTPtoSMSgw.SMS;

namespace SMTPtoSMSgw.SMTP
{
	class SmtpServer : IServer
	{
		private readonly TcpListener _listener;
		private readonly SmsSender _sender;
		private bool _continue = true;

		List<Task> _tasks = new List<Task>();

		public SmtpServer(TcpListener listener, SmsSender sender)
		{
			_listener = listener;
			_sender = sender;
		}

		Message message = new Message();

		//blocking operation
		public void Run()
		{
			Console.WriteLine("SMTP Start...");
			while (_continue)
			{
				_listener.Start();
				var client = _listener.AcceptTcpClient();
				client.Write("220 localhost -- HWR SMTP proxy server");
				Task t = Task.Run(() =>
				{
					while (true)
					{
						string data = client.Read();

						if (data.Trim().Length > 0)
						{
							Console.WriteLine("REC:" + data);

							if (data.StartsWith("QUIT"))
							{
								client.Close();
								break;
							}

							if (data.StartsWith("HELO"))
							{
								client.Write("250 OK");
							}

							if (data.StartsWith("EHLO"))
							{
								client.Write("250 OK");
							}

							if (data.StartsWith("RCPT TO"))
							{
								message.Number = data.Substring(8).Trim('>', '<').Split('@')[0];
								Console.WriteLine($"SMSReceiver:{data}");
								client.Write("250 OK");
							}

							if (data.StartsWith("MAIL FROM"))
							{
								client.Write("250 OK");
							}

							if (data.StartsWith("DATA"))
							{
								client.Write("354 End data with <CR><LF>.<CR><LF>");
								data = client.Read();
								Console.WriteLine("REC:" + data);
								message.Text = data.Split('\n').SingleOrDefault(s => s.StartsWith("Subject:"))?.Substring(8) ?? "";
								_sender.Send(message);
								client.Write("250 OK");
							}
						}
					}
				});
				_tasks.Add(t);
			}

			foreach (var task in _tasks)
			{
				task.Wait(5000);
			}

			_listener.Stop();
		}

		public void Stop() => _continue = false;


		
	}
}
