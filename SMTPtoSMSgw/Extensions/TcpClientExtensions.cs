﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SMTPtoSMSgw.Extensions
{
	public static class TcpClientExtensions
	{
		public static void Write(this TcpClient client, string data)
		{
			NetworkStream clientStream = client.GetStream();
			ASCIIEncoding encoder = new ASCIIEncoding();
			byte[] buffer = encoder.GetBytes(data + "\r\n");

			clientStream.Write(buffer, 0, buffer.Length);
			clientStream.Flush();
		}

		public static string Read(this TcpClient client)
		{
			byte[] messageBytes = new byte[8192];
			int bytesRead = 0;
			NetworkStream clientStream = client.GetStream();
			ASCIIEncoding encoder = new ASCIIEncoding();
			bytesRead = clientStream.Read(messageBytes, 0, 8192);
			string strMessage = encoder.GetString(messageBytes, 0, bytesRead);
			return strMessage;
		}
	}
}
